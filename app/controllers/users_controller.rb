class UsersController < ApplicationController
  include UsersHelper
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to action: 'show'
    else
      render action: 'new'
    end
  end

  def user_params
    params.require(:user).permit(:name, :related_user_ids => [])
  end
end
