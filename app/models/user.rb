class User < ApplicationRecord
  
  has_and_belongs_to_many :related_users,
              class_name: "User", 
              foreign_key: :user_id,
              join_table: "related_users_users",
              association_foreign_key: :related_user_id
end
