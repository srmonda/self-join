module UsersHelper
  def users
    User.all
  end

  def save_related(params)
    m = params[:user]
    a = m[:following_ids]
    b = [] 
    a.each do |id|
      if !id.blank?
        user = User.find(id)
        b << user
      end
    end
  end
end
