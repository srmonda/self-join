class CreateJoinTableUserRelatedUser < ActiveRecord::Migration[5.1]
  def change
    create_join_table :users, :related_users do |t|
      t.index [:user_id, :related_user_id]
      t.index [:related_user_id, :user_id]
    end
  end
end
